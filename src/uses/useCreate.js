import http from "@/utils/request";
import {ElLoading, ElMessage} from "element-plus";
import {useRouter} from "vue-router";

export function useCreate(model,form,ruleForm,back=true) {
    const router = useRouter();
    const createData = async () => {
        try {
            let valid = await ruleForm.value.validate();
            if (!valid) {
                return;
            }
        } catch (e) {
            return;
        }
        const loading = ElLoading.service({
            lock: true,
            text: '数据处理中',
            background: 'rgba(0, 0, 0, 0.7)',
        })
        let res = await http.post(`/${model}/create`, form.value);
        loading.close();
        if (res.code !== 200) {
            ElMessage.error(res.msg)
            return
        }
        ElMessage({
            message: '添加数据成功',
            type: 'success',
        })

        if (back){
            router.go(-1);
        }

    }
    const  goBack=()=> {
        router.go(-1);
    }
    return {createData,goBack}

}
