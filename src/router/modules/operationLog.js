export default [
    {
        name: "operationLog_home",
        path: "/operationLog/index",
        component: () => import("@/views/pages/operationLog/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "operationLog_add",
        path: "/operationLog/add",
        component: () => import("@/views/pages/operationLog/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "operationLog_update",
        path: "/operationLog/update",
        component: () => import("@/views/pages/operationLog/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "operationLog_layout",
        path: "/operationLog/view_layout",
        component: () => import("@/views/pages/operationLog/view_layout.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "operationLog_view",
                path: "/operationLog/view",
                component: () => import("@/views/pages/operationLog/view.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]
    }
]