export default [
    {
        name: "shopmaterialcatalog_home",
        path: "/shopmaterialcatalog/index",
        component: () => import("@/views/pages/shopmaterialcatalog/index.vue"),
        meta: {
            title: "商家素材分类管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopmaterialcatalog_add",
        path: "/shopmaterialcatalog/add",
        component: () => import("@/views/pages/shopmaterialcatalog/add.vue"),
        meta: {
            title: "添加商家素材分类",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopmaterialcatalog_update",
        path: "/shopmaterialcatalog/update",
        component: () => import("@/views/pages/shopmaterialcatalog/update.vue"),
        meta: {
            title: "更新商家素材分类",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopmaterialcatalog_view",
        path: "/shopmaterialcatalog/view",
        component: () => import("@/views/pages/shopmaterialcatalog/view.vue"),
        meta: {
            title: "商家素材分类详情",
            icon: "el-icon-platform-eleme"
        }
    },
]