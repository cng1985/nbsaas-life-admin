export default [
    {
        name: "explorationShop_home",
        path: "/explorationShop/index",
        component: () => import("@/views/pages/explorationShop/index.vue"),
        meta: {
            title: "商家管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "explorationShop_add",
        path: "/explorationShop/add",
        component: () => import("@/views/pages/explorationShop/add.vue"),
        meta: {
            title: "添加商家",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "explorationShop_update",
        path: "/explorationShop/update",
        component: () => import("@/views/pages/explorationShop/update.vue"),
        meta: {
            title: "更新商家",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "explorationShop_layout",
        path: "/explorationShop/view_layout",
        component: () => import("@/views/pages/explorationShop/view_layout.vue"),
        meta: {
            title: "商家详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "explorationShop_view",
                path: "/explorationShop/view",
                component: () => import("@/views/pages/explorationShop/view.vue"),
                meta: {
                    title: "商家详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]
