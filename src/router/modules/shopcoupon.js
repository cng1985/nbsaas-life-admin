export default [
    {
        name: "shopcoupon_home",
        path: "/shopcoupon/index",
        component: () => import("@/views/pages/shopcoupon/index.vue"),
        meta: {
            title: "商家管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopcoupon_add",
        path: "/shopcoupon/add",
        component: () => import("@/views/pages/shopcoupon/add.vue"),
        meta: {
            title: "添加商家",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopcoupon_update",
        path: "/shopcoupon/update",
        component: () => import("@/views/pages/shopcoupon/update.vue"),
        meta: {
            title: "更新商家",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopcoupon_layout",
        path: "/shopcoupon/view_layout",
        component: () => import("@/views/pages/shopcoupon/view_layout.vue"),
        meta: {
            title: "商家详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "shopcoupon_view",
                path: "/shopcoupon/view",
                component: () => import("@/views/pages/shopcoupon/view.vue"),
                meta: {
                    title: "商家详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]