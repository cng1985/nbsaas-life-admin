export default [
    {
        name: "queryTemplate_home",
        path: "/queryTemplate/index",
        component: () => import("@/views/pages/queryTemplate/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "queryTemplate_add",
        path: "/queryTemplate/add",
        component: () => import("@/views/pages/queryTemplate/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "queryTemplate_update",
        path: "/queryTemplate/update",
        component: () => import("@/views/pages/queryTemplate/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "queryTemplate_layout",
        path: "/queryTemplate/view_layout",
        component: () => import("@/views/pages/queryTemplate/view_layout.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "queryTemplate_view",
                path: "/queryTemplate/view",
                component: () => import("@/views/pages/queryTemplate/view.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]
    }
]