export default [
    {
        name: "shop_home",
        path: "/shop/index",
        component: () => import("@/views/pages/shop/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shop_add",
        path: "/shop/add",
        component: () => import("@/views/pages/shop/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shop_update",
        path: "/shop/update",
        component: () => import("@/views/pages/shop/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shop_layout",
        path: "/shop/view_layout",
        component: () => import("@/views/pages/shop/view_layout.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "shop_view",
                path: "/shop/view",
                component: () => import("@/views/pages/shop/view.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "view_role",
                path: "/shop/view_role",
                component: () => import("@/views/pages/shop/view_role.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "view_staff",
                path: "/shop/view_staff",
                component: () => import("@/views/pages/shop/view_staff.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]
    }
]
