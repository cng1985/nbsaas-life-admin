export default [
    {
        name: "model_home",
        path: "/model/index",
        component: () => import("@/views/pages/model/index.vue"),
        meta: {
            title: "会员管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "model_add",
        path: "/model/add",
        component: () => import("@/views/pages/model/add.vue"),
        meta: {
            title: "添加会员",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "model_update",
        path: "/model/update",
        component: () => import("@/views/pages/model/update.vue"),
        meta: {
            title: "更新会员",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "model_layout",
        path: "/model/view_layout",
        component: () => import("@/views/pages/model/view_layout.vue"),
        meta: {
            title: "会员详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "model_view",
                path: "/model/view",
                component: () => import("@/views/pages/model/view.vue"),
                meta: {
                    title: "会员详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "model_view_join",
                path: "/model/view_join",
                component: () => import("@/views/pages/model/view_join.vue"),
                meta: {
                    title: "报名的通告",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "model_view_score",
                path: "/model/view_score",
                component: () => import("@/views/pages/model/view_score.vue"),
                meta: {
                    title: "积分记录",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "model_view_money",
                path: "/model/view_money",
                component: () => import("@/views/pages/model/view_money.vue"),
                meta: {
                    title: "资金记录",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "model_view_frozen",
                path: "/model/view_frozen",
                component: () => import("@/views/pages/model/view_frozen.vue"),
                meta: {
                    title: "冻结记录",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]
