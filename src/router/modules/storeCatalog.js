export default [
    {
        name: "storeCatalog_home",
        path: "/storeCatalog/index",
        component: () => import("@/views/pages/storeCatalog/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    }
]