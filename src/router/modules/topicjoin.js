export default [
    {
        name: "topicjoin_home",
        path: "/topicjoin/index",
        component: () => import("@/views/pages/topicjoin/index.vue"),
        meta: {
            title: "话题管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "topicjoin_add",
        path: "/topicjoin/add",
        component: () => import("@/views/pages/topicjoin/add.vue"),
        meta: {
            title: "添加话题",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "topicjoin_update",
        path: "/topicjoin/update",
        component: () => import("@/views/pages/topicjoin/update.vue"),
        meta: {
            title: "更新话题",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "topicjoin_view",
        path: "/topicjoin/view",
        component: () => import("@/views/pages/topicjoin/view.vue"),
        meta: {
            title: "话题详情",
            icon: "el-icon-platform-eleme"
        }
    },
]