export default [
    {
        name: "dict_home",
        path: "/dict/index",
        component: () => import("@/views/pages/dict/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    }
]
