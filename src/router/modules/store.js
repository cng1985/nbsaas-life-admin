export default [
    {
        name: "store_home",
        path: "/store/index",
        component: () => import("@/views/pages/store/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "store_add",
        path: "/store/add",
        component: () => import("@/views/pages/store/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "store_update",
        path: "/store/update",
        component: () => import("@/views/pages/store/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "store_layout",
        path: "/store/view_layout",
        component: () => import("@/views/pages/store/view_layout.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "store_view",
                path: "/store/view",
                component: () => import("@/views/pages/store/view.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]
    }
]