export default [
    {
        name: "certdata_home",
        path: "/certdata/index",
        component: () => import("@/views/pages/certdata/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "certdata_add",
        path: "/certdata/add",
        component: () => import("@/views/pages/certdata/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "certdata_update",
        path: "/certdata/update",
        component: () => import("@/views/pages/certdata/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "certdata_view",
        path: "/certdata/view",
        component: () => import("@/views/pages/certdata/view.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    },
]