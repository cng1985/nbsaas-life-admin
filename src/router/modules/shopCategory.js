export default [
    {
        name: "shopCategory_home",
        path: "/shopCategory/index",
        component: () => import("@/views/pages/shopCategory/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    }
]
