export default [
    {
        name: "menu_home",
        path: "/menu/index",
        component: () => import("@/views/common/setting/menu/index.vue"),
        meta: {
            title: "菜单管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "role_home",
        path: "/role/index",
        component: () => import("@/views/common/setting/role/index.vue"),
        meta: {
            title: "角色管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "user_home",
        path: "/user/index",
        component: () => import("@/views/common/setting/user/index.vue"),
        meta: {
            title: "用户管理",
            icon: "el-icon-platform-eleme"
        }
    }
]
