export default [
    {
        name: "tenantdictionary_home",
        path: "/tenantdictionary/index",
        component: () => import("@/views/common/tenantdictionary/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantdictionary_add",
        path: "/tenantdictionary/add",
        component: () => import("@/views/common/tenantdictionary/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantdictionary_update",
        path: "/tenantdictionary/update",
        component: () => import("@/views/common/tenantdictionary/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantdictionary_view",
        path: "/tenantdictionary/view",
        component: () => import("@/views/common/tenantdictionary/view.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    },
]