export default [
    {
        name: "tenantoauthconfig_home",
        path: "/tenantoauthconfig/index",
        component: () => import("@/views/common/tenantoauthconfig/index.vue"),
        meta: {
            title: "第三方登陆管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantoauthconfig_add",
        path: "/tenantoauthconfig/add",
        component: () => import("@/views/common/tenantoauthconfig/add.vue"),
        meta: {
            title: "添加第三方登陆",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantoauthconfig_update",
        path: "/tenantoauthconfig/update",
        component: () => import("@/views/common/tenantoauthconfig/update.vue"),
        meta: {
            title: "更新第三方登陆",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantoauthconfig_layout",
        path: "/tenantoauthconfig/view_layout",
        component: () => import("@/views/common/tenantoauthconfig/view_layout.vue"),
        meta: {
            title: "第三方登陆详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "tenantoauthconfig_view",
                path: "/tenantoauthconfig/view",
                component: () => import("@/views/common/tenantoauthconfig/view.vue"),
                meta: {
                    title: "第三方登陆详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]