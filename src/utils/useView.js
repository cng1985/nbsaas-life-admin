import {useRouter} from "vue-router";

export function useView(functionMethod,getSearchList) {

    const router = useRouter();

    const showView=async (row) => {
        let data = {};
        data.id = row.id;
        await router.push({
            path: 'view',
            query: data
        });
    }
    const addView=async (row) => {
        await router.push({
            path: 'add'
        });
    }
    const editView=async (row) => {
        let data = {};
        data.id = row.id;
        await router.push({
            path: 'update',
            query: data
        });
    }
    const  goBack=()=> {
         router.go(-1);
    }
    return {showView,addView,editView,goBack}

}