import {onMounted, ref} from "vue";
import http from "@/utils/request";

export function usePage(functionMethod, searchObject) {

    let pageData = ref({
        size:10,
        total:0,
        totalPage:0,
        data:[]
    })

    let loading = ref(false)

    const searchList = async () => {
        loading.value = true;
        let res = await http.post(functionMethod, searchObject);
        if (res.code === 200) {
            pageData.value = res;
        }
        loading.value = false;
    }
    const sizeChange = async (event) => {
        searchObject.size = event;
        await searchList();
    }


    const pageChange = async (index) => {
         searchObject.no = index;
        await searchList();
    }
    const search = async () => {
        searchObject.no = 1;
        await searchList();
    }

    const changeTableSort = async (column) => {
        searchObject.sortField = column.prop;
        if ("descending" === column.order) {
            searchObject.sortMethod = "desc";
        } else if ("ascending" === column.order) {
            searchObject.sortMethod = "asc";
        } else {
            searchObject.sortMethod = "";
        }
        await searchList();
    }

    onMounted(async () => {
        await search();
    })

    return {pageData, searchList, sizeChange, pageChange, search, changeTableSort,loading}

}
