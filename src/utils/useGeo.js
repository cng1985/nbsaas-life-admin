import http from "@/utils/request";

export function useGeo(form) {

    const geo = async () => {
        let param = {};
        param.address = form.value.address;
        let res = await http.form("/area/lbs", param);
        if (res.code === 200) {
            let result = res.data;
            form.value.lat = result.lat;
            form.value.lng = result.lng;
            if (result.provinceId) {
                form.value.province = result.provinceId;
            }
            if (result.cityId) {
                form.value.city = result.cityId;
            }
            if (result.countyId) {
                form.value.county = result.countyId;
            }
        }
    }
    const changePoi = (event) => {
        console.log(event);
        form.value.lat = event.lat;
        form.value.lng = event.lng;
    }
    return {geo, changePoi}

}
