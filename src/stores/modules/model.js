import { defineStore } from 'pinia'

export const modelStore = defineStore('modelStore', {

    state: () => {
        return { searchObject: {
                no: 1,
                size: 10,
                name: '',
                phone: '',
                cardNo: ''
            } }
    },
    getters: {
        searchData: (state) => state.searchObject,
    },
    actions: {
        updateSearchObject(obj){
            this.searchObject=obj;
        }
    },
})
