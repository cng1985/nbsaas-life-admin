import { defineStore } from 'pinia'

export const systemStore = defineStore('systemStore', {

    state: () => {
        return { searchObject: {
                no: 1,
                size: 10,
                name: '',
                phone: '',
                cardNo: '',
                dataType:"1000"
            } }
    },
    getters: {
        searchData: (state) => state.searchObject,
    },
    actions: {
        updateSearchObject(obj){
            this.searchObject=obj;
        }
    },
})
