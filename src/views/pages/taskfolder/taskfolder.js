export default [
    {
        name: "taskfolder_home",
        path: "/taskfolder/index",
        component: () => import("@/views/pages/taskfolder/index.vue"),
        meta: {
            title: "任务夹管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "taskfolder_add",
        path: "/taskfolder/add",
        component: () => import("@/views/pages/taskfolder/add.vue"),
        meta: {
            title: "添加任务夹",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "taskfolder_update",
        path: "/taskfolder/update",
        component: () => import("@/views/pages/taskfolder/update.vue"),
        meta: {
            title: "更新任务夹",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "taskfolder_layout",
        path: "/taskfolder/view_layout",
        component: () => import("@/views/pages/taskfolder/view_layout.vue"),
        meta: {
            title: "任务夹详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "taskfolder_view",
                path: "/taskfolder/view",
                component: () => import("@/views/pages/taskfolder/view.vue"),
                meta: {
                    title: "任务夹详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]